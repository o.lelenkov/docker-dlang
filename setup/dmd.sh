#!/bin/sh
set -e
curl http://downloads.dlang.org/releases/2.x/${DMD_VERSION}/dmd_${DMD_VERSION}-0_amd64.deb -L -o /tmp/dmd.deb \
&& dpkg -i /tmp/dmd.deb \
&& rm -rf /tmp/dmd.deb

