#!/bin/sh
set -e

rm -rf /usr/bin/dub
cd /tmp/
git clone https://github.com/dlang/dub.git -b ${DUB_VERSION}
cd /tmp/dub
./build.sh
cp /tmp/dub/bin/dub /usr/bin
cd /
rm -rf /tmp/dub

