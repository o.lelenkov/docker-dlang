export VERSION=$(shell cat VERSION)
export MAJOR=$(shell cat VERSION | awk -F '.' '{print $$1}')
export MINOR=$(shell cat VERSION | awk -F '.' '{print $$2}')
export BUILD=$(shell cat VERSION | awk -F '.' '{print $$3}')

ifndef CI_REGISTRY_IMAGE
	CI_REGISTRY_IMAGE := "registry.gitlab.com/o.lelenkov/docker-dlang"
endif

all: build

build-dev:
	@docker build --target dev --tag=${CI_REGISTRY_IMAGE}/dev:${VERSION} .

build-runtime:
	@docker build --target runtime \
		--build-arg MAJOR=${MAJOR} \
		--build-arg MINOR=${MINOR} \
		--build-arg BUILD=${BUILD} \
		--tag=${CI_REGISTRY_IMAGE}/runtime:${VERSION} .

build: build-dev build-runtime

release-dev: build-dev
	@docker tag ${CI_REGISTRY_IMAGE}/dev:${VERSION} \
		${CI_REGISTRY_IMAGE}/dev:latest

release-runtime: build-runtime
	@docker tag ${CI_REGISTRY_IMAGE}/runtime:${VERSION} \
		${CI_REGISTRY_IMAGE}/runtime:latest

release: release-dev release-runtime

publish-dev: release-dev
	@docker push ${CI_REGISTRY_IMAGE}/dev:${VERSION}
	@docker push ${CI_REGISTRY_IMAGE}/dev:latest

publish-runtime: release-runtime
	@docker push ${CI_REGISTRY_IMAGE}/runtime:${VERSION}
	@docker push ${CI_REGISTRY_IMAGE}/runtime:latest

publish: publish-dev publish-runtime

