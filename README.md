[![build status](https://gitlab.com/o.lelenkov/docker-dlang/badges/master/build.svg)](https://gitlab.com/o.lelenkov/docker-dlang/commits/master)

Образы содержат минимальный набор инструментов для разработки на языке [D](http://dlang.org/) 
и запуска приложений.

## Состав

* DMD
* LDC
* DUB

