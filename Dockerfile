# * DEV *
FROM registry.gitlab.com/o.lelenkov/docker-minideb:buster-0.2 AS dev
MAINTAINER Oleg Lelenkov "o.lelenkov@gmail.com"

ARG DMD_VERSION=2.097.2
ARG LLVM_VERSION=12
ARG LDC_COMMIT=v1.27.1
ARG DUB_VERSION=v1.23.0

RUN install_packages curl libc6-dev gcc wget \
        gnupg2 ca-certificates netbase git \
        cmake make g++ zlib1g-dev

COPY ./setup /tmp/setup
# install DMD
RUN sh /tmp/setup/dmd.sh
# install ldc2
RUN sh /tmp/setup/ldc2.sh
# install dub
RUN sh /tmp/setup/dub.sh
# clear
RUN apt-get -y autoremove && rm -rf /var/lib/apt/lists/* \
        && rm -rf /tmp/* && rm -rf ~/.dub

# * RUNTIME *
FROM registry.gitlab.com/o.lelenkov/docker-minideb:buster-0.2 AS runtime
MAINTAINER Oleg Lelenkov "o.lelenkov@gmail.com"

ARG MAJOR
ARG MINOR
ARG BUILD

ARG LDC_VER=${MAJOR}.0.${MINOR}
ARG DMD_VER=0.${MINOR}.${BUILD}

COPY --from=dev /usr/local/lib/libdruntime-ldc-debug-shared.so.${LDC_VER} \
                /usr/local/lib/libdruntime-ldc-debug-shared.so.${LDC_VER}
COPY --from=dev /usr/local/lib/libdruntime-ldc-shared.so.${LDC_VER} \
                /usr/local/lib/libdruntime-ldc-shared.so.${LDC_VER}

COPY --from=dev /usr/local/lib/libphobos2-ldc-debug-shared.so.${LDC_VER} \
                /usr/local/lib/libphobos2-ldc-debug-shared.so.${LDC_VER}
COPY --from=dev /usr/local/lib/libphobos2-ldc-shared.so.${LDC_VER} \
                /usr/local/lib/libphobos2-ldc-shared.so.${LDC_VER}

COPY --from=dev /usr/lib/x86_64-linux-gnu/libphobos2.so.${DMD_VER} \
                /usr/lib/x86_64-linux-gnu/libphobos2.so.${DMD_VER}

RUN cd /usr/local/lib && \
    for lib in "libdruntime-ldc-debug" "libdruntime-ldc" "libphobos2-ldc-debug" "libphobos2-ldc"; do \
        ln -s ${lib}.so.${MAJOR}.0.${MINOR} ${lib}.so.${MINOR}; \
        ln -s ${lib}.so.${MINOR} ${lib}.so; \
    done;

RUN cd /usr/lib/x86_64-linux-gnu \
    && ln -s libphobos2.so.0.${MINOR}.${BUILD} libphobos2.so.0.${MINOR} \
    && ln -s libphobos2.so.0.${MINOR} libphobos2.so

RUN install_packages libcurl4

